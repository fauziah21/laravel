<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Halaman Form</title>
  </head>
  <body>

    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <!--membuat form-->
    <form action="/welcome" method="post">
      @csrf
      <label for="FirstName">First Name:</label> <br><br>
      <input type="text" name="nama_depan" id="FirstName"> <br> <br>
      <label for="LastName" >Last Name:</label> <br><br>
      <input type="text" name="nama_belakang" id="LastName"><br> <br>

      <!--Jenis kelamin-->
      <label for="gender">Gender:</label> <br><br>
      <input type="radio" name="gender" value="0"> Male <br>
      <input type="radio" name="gender" value="1"> Female <br>
      <input type="radio" name="gender" value="2"> Other <br>
      <br>

      <!--Nationality-->
      <label>Nationality: </label><br><br>
      <select >
        <option>Indonesian</option>
        <option>Australian</option>
        <option>Thailand</option>
      </select>
      <br><br>

      <!--bahasa-->
      <label for="">Language Spoken: </label><br><br>
      <input type="checkbox" name="bahasa" value="0">Bahasa Indonesia <br>
      <input type="checkbox" name="bahasa" value="1">English <br>
      <input type="checkbox" name="bahasa" value="2">Other <br>
      <br>

      <label for="">Bio: </label><br><br>
      <textarea id="bio" rows="10" cols="30"></textarea><br>
      <input type="submit" value="Sign Up">
    </form >

  </body>
</html>
